/**
 * Keeping track of problems and solutions as I work through them, see https://projecteuler.net
 * Tony Rivas
 */

/**
 * Problem 4, Largest palindrome product
 * First create a palindrome function
 * Second generate product numbers to feed into the palindrome function
 * This solution allows user to find more than 3 digits and combinations e.g. first number is 2 digits and second is 3
 */
function problem4 (digits1 = 2, digits2 = 2) {
    function isPalindrome (word) {
        const wordArray = word.replace(/[^a-zA-Z0-9]+/g, '').split('') // get only alphanumeric
        const length = wordArray.length
        const limit = (length % 2 === 0 && length > 1) ? length / 2 : (length - 1) / 2
        let found = true
        if (length === 1) {
            return found
        }
        else {
            // loop through first half of word
            for (let i = 0; i < limit; i++) {
                if (wordArray[i] !== wordArray[wordArray.length - (i + 1)]) {
                    found = false
                    break
                }
            }
        }
        return found
    }
    function minMax (digits) {
        return {
            min: parseInt('1'.padEnd(digits, '0')),
            max: parseInt('9'.padEnd(digits, '9'))
        }
    }
    const d1 = minMax(digits1)
    const d2 = minMax(digits2)
    let palindrome = null
    // search through numbers starting at max, and decrement e.g. 2 three digit numbers -> 999 X 999
    for (let i = d1.max; i >= d1.min; i-- ) {
        for (let j = d2.max; j >= d2.min; j--) {
            const str = String(i * j)
            if (isPalindrome(str)) {
                if (!palindrome || (palindrome && (parseInt(str) > parseInt(palindrome)))) {
                    // store only the largest number
                    // TODO: figure out a reasonable break, for example break after finding 10 more palindromes but they are
                    // less than the highest stored one. for performance
                    palindrome = parseInt(str)
                }
            }
        }
    }
    return palindrome
}

/**
 * Problem 3, Largest prime factor
 * A prime number is a number greater than 1 and whose factors are itself and 1. 2 is the only even number,
 * the rest are odds.
 * Look for factors first then determine if they are prime numbers
 */
function problem3 (num = 13195) {
    let primeFactors = []
    const SMALLEST_PRIME_FACTOR = 2
    let primeFactorsFound = false // flag to stop loop when the last factor is found, performance
    if (num < SMALLEST_PRIME_FACTOR) {
        // console.log('Use a number larger than 1.')
        return -1
    }
    for (let i = SMALLEST_PRIME_FACTOR; (i <= num && !primeFactorsFound); i++) {
        if (num % i === 0) { // it's a factor
            const isEven = i % 2 === 0
            let j = isEven ? 2 : 3
            let factorsFound = []
            for (j; j <= i; j += 2) {
                if (i % j === 0) {
                    factorsFound.push(j)
                }
            }
            const factorsFoundLength = factorsFound.length
            const primeFactorsLength = primeFactors.length
            if (factorsFoundLength) {
                if (factorsFoundLength === 1) {
                    // it's a prime factor, divisible by 1 and itself
                    primeFactors.push(factorsFound[0])
                }
                if (factorsFoundLength > 1 && primeFactorsLength && factorsFound[0] === primeFactors[primeFactorsLength - 1] ) {
                    // the same prime factor was found again consecutively
                    // 3 x 3 x 17 ...
                    primeFactors.push(factorsFound[0])
                }
            }
        }
        let factorSum = 1
        if (primeFactors.length) {
            primeFactors.forEach(factor => (
                factorSum *= factor
            ))
            // stop counting after all prime factors have been found
            // they should all sum up to equal the number provided
            if (factorSum === num) {
                primeFactorsFound === true
                break
            }
            if (factorSum > num) {
                // for debugging
                console.log('Error: prime factors not found')
                return primeFactors
            }
        }
    }
    return primeFactors[primeFactors.length - 1]
}


/**
 * Problem 2, Even Fibonacci numbers
 * Using a classical fibonacci sequence algorithm to start. From there add logic to stop loop when even's sum
 * exceeded 4Mil. Got the right answer at first but wasn't sure if the value before exceeding 4Mil (1089154)
 * should be returned instead
 */
function problem2 (limit = 4000000) {
    let fibSum = 0
    let evensSum = 0
    let v1 = 0
    let v2 = 1
    for (let i = 0; evensSum < limit; i++) {
        fibSum = v1 + v2
        if (fibSum % 2 === 0) {
            evensSum += fibSum
        }
        v1 = v2
        v2 = fibSum
    }
    return evensSum
}
problem2(4000000)

/**
 * Problem 1, Multiples of 3 or 5
 * Originally, my code evaluated multiples of 3 and 5 separately and summed them. The problem with that is there
 * are duplicates that will get double counted. For example both 3 and 5 are both multiples of 300. Found best solution
 * here for trouble shooting https://projecteuler.net/thread=1;page=9
    let sum = 0;
    for (let i = 0; i < 10; i++) {
        if (i % 3 === 0 || i % 5 === 0) {
            sum += i
        }
    }
    return sum
 * Decided to salvage code to track learning.
*/
function problem1 (m1 = 3, m2 = 5, l1 = 10) {
    function sumTheMultiples (multiple, limit) {
        let numberOfMultiples = Math.floor(limit / multiple) - (limit % multiple > 0 ? 0 : 1)
        let count = 0
        let sum = []
        while (numberOfMultiples > count) {
            sum.push(multiple * (count + 1))
            count++
        }
        return sum
    }
    return [...new Set([...sumTheMultiples(m1, l1), ...sumTheMultiples(m2, l1)])].reduce((a, b) => a + b, 0)
}

module.exports = {
    problem4,
    problem3,
    problem2,
    problem1
}
