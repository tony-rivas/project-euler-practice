const expect = require('chai').expect
const pE = require('./projectEuler')

describe ('Project Euler Practice Problems', () => {

    it ('problem 4', () => {
        expect(pE.problem4(1, 1)).eq(9)
        expect(pE.problem4(2, 2)).eq(9009)
        expect(pE.problem4(3, 3)).eq(906609)
        expect(pE.problem4(2, 3)).eq(94149)
    })

    it ('problem 3', () => {
        expect(pE.problem3(0)).eq(-1, 'error return 0')
        expect(pE.problem3(2)).eq(2, 'prime factors: 2')
        expect(pE.problem3(13195)).eq(29, 'prime factors: 5 x 7 x 13 x 29')
        expect(pE.problem3(96993)).eq(829, 'prime factors: 3 x 3 x 13 x 829')
        expect(pE.problem3(9041)).eq(9041, 'prime factors: 9041')
        expect(pE.problem3(9699368)).eq(457, 'prime factors: 2 x 2 x 2 x 7 x 379 x 457')
        expect(pE.problem3(600851475143)).eq(6857, 'prime factors: 71 x 839 x 1471 x 6857')
    })

    it ('problem 2', () => {
        expect(pE.problem2(4000000)).eq(4613732)
    })

    it ('problem 1', () => {
        expect(pE.problem1(3, 5, 10)).eq(23)
        expect(pE.problem1(3, 5, 1000)).eq(233168)
        expect(pE.problem1(7, 9, 222)).eq(5794)
    })
})
